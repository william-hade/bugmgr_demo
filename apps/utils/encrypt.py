import hashlib
from django.conf import settings


def md5(string):
    # 为加密设置一个密钥
    hash_object = hashlib.md5(settings.SECRET_KEY.encode("utf-8"))
    # 加密传入的字符
    hash_object.update(string.encode("utf-8"))
    # 返回
    return hash_object.hexdigest()
