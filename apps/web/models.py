from django.db import models


# Create your models here.

# app_userinfo
class UserInfo(models.Model):
    login_name = models.CharField(verbose_name="登录名",   max_length=100, db_index=True)  # 登陆名字段
    username = models.CharField(verbose_name="姓名", max_length=100)  # 姓名
    email = models.EmailField(verbose_name="邮箱", max_length=200)  # 邮箱地址
    mobile_phone = models.CharField(verbose_name="手机号码", max_length=100)  # 手机号码
    password = models.CharField(verbose_name="密码", max_length=500)  # 密码

    class Meta:
        managed = True

    def __str__(self):
        return "登陆名：%s\t用户名：%s" % (self.login_name, self.username)
