"""
 首页相关的views
"""
# 引入render
from django.shortcuts import render
# 引入models中的对象
from web.models import UserInfo


def index(request):
    """首页"""
    # 加载页面
    return render(request, 'home.html')
